import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:presensi_uts/home-page.dart';
import 'package:http/http.dart' as myHttp;
import 'package:presensi_uts/models/login-response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget{
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>{
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  late Future<String> _name, _token;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _token = _prefs.then((SharedPreferences prefs) {
      return prefs.getString("token") ?? "";
    });

    _name = _prefs.then((SharedPreferences prefs) {
      return prefs.getString("name") ?? "";
    });
    checkToken(_token, _name);
  }

  Future saveUser(token, name) async {
    try {
      print("LEWAT SINI" + token + " | " + name);
      final SharedPreferences pref = await _prefs;
      pref.setString("name", name);
      pref.setString("token", token);
    } catch(err) {
      print('ERROR : '+err.toString());
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(err.toString())));
    }

  }

  checkToken(token, name) async {
    String tokenStr = await token;
    String nameStr = await name;
    if(tokenStr != "" && nameStr != "") {
      Future.delayed(Duration(seconds: 1), () async {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => HomePage()))
            .then((value) {
          setState(() {});
        });
      });
    }
  }
  Future login(email, password) async {
    LoginResponseModel? loginResponseModel;
    Map<String, String> body = {"email": email, "password": password};
    final headers = {'Content-Type': 'application/json'};
    var response = await myHttp.post(Uri.parse('http://127.0.0.1:8000/api/login'),
        body: body);
    if (response.statusCode == 401) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Email atau Password salah")));
    }else {
      loginResponseModel = LoginResponseModel.fromJson(json.decode(response.body));
      print('HASIL : '+ response.body);
      saveUser(loginResponseModel.data.token, loginResponseModel.data.name);
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(child: Text("Login")),
                SizedBox(height: 20),
                Text("Email"),
                TextField(controller: emailController,),
                SizedBox(height: 20),
                Text("Password"),
                TextField(
                  controller: passwordController,
                  obscureText: true, //agar password tertutup
                ),
                SizedBox(height: 20),
                ElevatedButton(onPressed: () {
                  login(emailController.text, passwordController.text);
                }, child: Text("Masuk"))
              ],
            ),
          ),
        ),
      ),
    );
  }
}


